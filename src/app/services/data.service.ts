import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  constructor(private http:HttpClient) { }
  
  login(data) {
    return this.http.post('https://server.kodeshode.com/api/login', data)
  }

  createuser(data: any){
    return this.http.post('http://13.244.115.67/server/api/registeruser',data);
  }
  addorg(data: any){
    return this.http.post('http://13.244.115.67/server/api/createneworg',data);
  }
  getorg(id){ 
    return this.http.get('http://13.244.115.67/server/api/getallorganization/'+id);
  }
  deleteorg(id){
    return this.http.get('http://13.244.115.67/server/api/deleteorganization/'+id);
  }
  addevent(data: any){
    return this.http.post('http://13.244.115.67/server/api/createnewevent',data);
  }
  getallevents(id){ 
    return this.http.get('http://13.244.115.67/server/api/getallevent/'+id);
  }
  getevents(id){ 
    return this.http.get('http://13.244.115.67/server/api/getevents/'+id);
  }
  showtickets(id){ 
    return this.http.get('http://13.244.115.67/server/api/showtickets/'+id);
  }
  addticket(data: any){
    return this.http.post('http://13.244.115.67/server/api/addticket',data);
  }
  showreceptions(id){ 
    return this.http.get('http://13.244.115.67/server/api/showreceptions/1');
  }
  showstages(id){ 
    return this.http.get('http://13.244.115.67/server/api/showstages/1');
  }
  showsessions(id){ 
    return this.http.get('http://13.244.115.67/server/api/getallsessions/1');
  }
  addreception(data: any){
    return this.http.post('http://13.244.115.67/server/api/addreceptiondata',data);
  }
  addstage(data: any){
    return this.http.post('http://13.244.115.67/server/api/addstage',data);
  }
  addsession(data: any){
    return this.http.post('http://13.244.115.67/server/api/addsession',data);
  }
  showlandingpage(id){ 
    return this.http.get('http://13.244.115.67/server/api/showlandingpage/1');
  }
  addlandingpage(data: any){
    return this.http.post('http://13.244.115.67/server/api/addlandingpage',data);
  }
  singlelandingpage(id){
    return this.http.get('http://13.244.115.67/server/api/singlelandingpage/'+id);
  }
  updatelandingpage(id,data){
    return this.http.post('http://13.244.115.67/server/api/updatelandingpage/'+id,data);
  }
  showexpo(id){ 
    return this.http.get('http://13.244.115.67/server/api/showexpo/1');
  }
  addexpo(data: any){
    return this.http.post('http://13.244.115.67/server/api/addexpo',data);
  }
  addrepresentative(data: any){
    return this.http.post('http://13.244.115.67/server/api/addrepresentative',data);
  }
  

  addspeaker(data: any){
    return this.http.post('http://13.244.115.67/server/api/addspeaker',data);
  }
  showspeaker(id){ 
    return this.http.get('http://13.244.115.67/server/api/showspeaker/13');
  }
  singlespeaker(id){
    return this.http.get('http://13.244.115.67/server/api/singlespeaker/'+id);
  }
  updatespeaker(id,data){
    return this.http.post('http://13.244.115.67/server/api/updatespeaker/'+id,data);
  }
  deletespeaker(id){
    return this.http.get('http://13.244.115.67/server/api/deletespeaker/'+id);
  }
  
  addsponsor(data: any){
    return this.http.post('http://13.244.115.67/server/api/addsponsor',data);
  }
  showsponsor(id){ 
    return this.http.get('http://13.244.115.67/server/api/showsponsor/13');
  }
  singlesponsor(id){
    return this.http.get('http://13.244.115.67/server/api/singlesponsor/'+id);
  }
  updatesponsor(id,data){
    return this.http.post('http://13.244.115.67/server/api/updatesponsor/'+id,data);
  }
  deletesponsor(id){
    return this.http.get('http://13.244.115.67/server/api/deletesponsor/'+id);
  }
  addattendee(data: any){
    return this.http.post('http://13.244.115.67/server/api/addattendee',data);
  }
  showattendee(id){ 
    return this.http.get('http://13.244.115.67/server/api/showattendee/13');
  }
  singleattendee(id){
    return this.http.get('http://13.244.115.67/server/api/singleattendee/'+id);
  }
  updateattendee(id,data){
    return this.http.post('http://13.244.115.67/server/api/updateattendee/'+id,data);
  }
  deleteattendee(id){
    return this.http.get('http://13.244.115.67/server/api/deleteattendees/'+id);
  }
}

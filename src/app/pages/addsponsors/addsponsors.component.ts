import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addsponsors',
  templateUrl: './addsponsors.component.html',
  styleUrls: ['./addsponsors.component.scss']
})
export class AddsponsorsComponent implements OnInit {

  public sponsor = {
	
    sponsor_name: null,
    sponsor_link: null,
    choose_order: null,
    sponsor_logo: null,
      event_id:1,
      managerid:1
    }
    
    userDisplayID='';
    sponsorsForm: FormGroup;
    files: any;
    constructor(private dataService:DataService,private router: Router,) { }
  
    ngOnInit(): void {
      this.sponsorsForm = new FormGroup({
        sponsor_name : new FormControl(),
        sponsor_link : new FormControl(),
        choose_order : new FormControl(),
        sponsor_logo : new FormControl(),
        event_id: new FormControl(13),
        managerid: new FormControl(5),
     });
    }
    addsponsors()
    {
  
      this.sponsor = this.sponsorsForm.value;
      let formdata = new FormData;
      formdata.append("sponsor_name",this.sponsorsForm.get('sponsor_name').value);
      formdata.append("sponsor_link",this.sponsorsForm.get('sponsor_link').value);
      formdata.append("choose_order",this.sponsorsForm.get('choose_order').value);
      formdata.append("sponsor_logo",this.sponsorsForm.get('sponsor_logo').value);
      formdata.append("event_id",this.sponsorsForm.get('event_id').value);
      formdata.append("managerid",this.sponsorsForm.get('managerid').value);
   
      this.dataService.addsponsor(formdata).subscribe(res=>{
          console.log(res);
          return this.router.navigateByUrl('/pages/sponsors');
      })
       
    }
    imageupload(event){
     if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.sponsorsForm.get('sponsor_logo').setValue(file);
      console.log(this.sponsorsForm.get('sponsor_logo').value);
    }
  }
}

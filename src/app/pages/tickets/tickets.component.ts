import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {
  arrtickets: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      ticket_name: {
        title: 'Name',
        type: 'string',
      },
      ticket_type: {
        title: 'Privacy',
        type: 'string',
      },
      ticket_price: {
        title: 'Price',
        type: 'number',
      },
      ticket_count: {
        title: 'Quantity',
        type: 'number',
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }
   ngOnInit(): void {
    this.showtickets(this.userDisplayID);
  }
  showtickets(userDisplayID){
   
    this.dataService.showtickets( this.userDisplayID).subscribe(res=>{
      this.arrtickets = res;
	  console.log(this.arrtickets);
  })
  }
  updateticket(event) {

    var data = {"ticket_name": event.newData.ticket_name,
                "ticket_type" : event.newData.ticket_type,
                "ticket_price" : event.newData.ticket_price,
                "ticket_count" : event.newData.ticket_count,
                "org_id":"1",
				"event_id":"1",
				"managerid":"1"
                };
                console.log(data);
  return this.http.post('https://server.kodeshode.com/api/updateticket/'+event.newData.id, data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.showtickets(this.userDisplayID);
      });
  }
  deleteTicket(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('https://server.kodeshode.com/api/deleteticket/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showtickets(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
    
   //event.confirm.resolve(event.source.data);

 }
}

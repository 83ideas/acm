import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addlandingpage',
  templateUrl: './addlandingpage.component.html',
  styleUrls: ['./addlandingpage.component.scss']
})
export class AddlandingpageComponent implements OnInit {
  
  public landing = {
    description : null,
    event_image : null,
    org_id:1,
    event_id:1,
    managerid:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  landingForm: FormGroup;
  files: any;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.landingForm = new FormGroup({
      description : new FormControl(),
      event_image : new FormControl(),
      org_id: new FormControl(1),
      event_id: new FormControl(1),
      managerid: new FormControl(1),
   });
  }
  addlandingpage()
  {

    this.landing = this.landingForm.value;
    let formdata = new FormData;
    formdata.append("event_image",this.landingForm.get('event_image').value);
    formdata.append("description",this.landingForm.get('description').value);
    formdata.append("org_id",this.landingForm.get('org_id').value);
    formdata.append("event_id",this.landingForm.get('event_id').value);
    formdata.append("managerid",this.landingForm.get('managerid').value);
   // formdata.append("data",JSON.stringify(this.landing));
    this.dataService.addlandingpage(formdata).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/landingpage');
    })
     
  }
  imageupload(event){
    this.files=event.target.files[0];
    this.landingForm.get('event_image').setValue(this.files);
}
}

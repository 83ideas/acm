import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdaterepresentativesComponent } from './updaterepresentatives.component';

describe('UpdaterepresentativesComponent', () => {
  let component: UpdaterepresentativesComponent;
  let fixture: ComponentFixture<UpdaterepresentativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdaterepresentativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdaterepresentativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

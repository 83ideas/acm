import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-networking',
  templateUrl: './networking.component.html',
  styleUrls: ['./networking.component.scss']
})
export class NetworkingComponent implements OnInit {

 
  [x: string]: any;
 
  userDisplayID = '';
  public loggedIn: boolean;
  constructor(private dataService:DataService) { }
  arrnetwork: any=[];
 
  ngOnInit(): void {
    this.shownetwork(this.userDisplayID);
  }
  shownetwork(userDisplayID){
    this.dataService.shownetwork(this.userDisplayID).subscribe(res=>{
     var response2 = res;
  
      console.log( response2 );
      this.arrnetwork = response2;
  })
  }
  deletenetwork(id){
    this.dataService.deletenetwork(id).subscribe(res=>{
      this.shownetwork(this.userDisplayID);
      console.log(id);
    })
    
  }

}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {

  arrlanding: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      Actions: //or something
      {
        type:'html',
        valuePrepareFunction:(cell,row)=>{
          return `<a title="edit" href="pages/updatelanding/${row.id}"> <i class="nb-edit"></i></a>`
        },
        filter:false       
      },
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      event_image: {
        title: 'Event Image',
        type: 'html',
        valuePrepareFunction: (images) => {
          return `<img class='table-thumbnail-img' width="50px" height="50px" src="https://server.kodeshode.com/public/images/${images}"/>`
        }
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }
   ngOnInit(): void {
    this.showlandingpage(this.userDisplayID);
  }
  showlandingpage(userDisplayID){
   
    this.dataService.showlandingpage( this.userDisplayID).subscribe(res=>{
      this.arrlanding = res;
	  console.log(this.arrlanding);
  })
  }
  updatelandingpage(event) {

    var data = {"description": event.newData.description,
                "event_image" : event.newData.event_image,
                "org_id":"1",
                "event_id":"1",
                "managerid":"1"
                };
                console.log(data);
  return this.http.post('https://server.kodeshode.com/api/updatelandingpage/'+event.newData.id, data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.showlandingpage(this.userDisplayID);
      });
  }
  deletelandingpage(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('https://server.kodeshode.com/api/deletelandingpage/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showlandingpage(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
    
   //event.confirm.resolve(event.source.data);

 }
}

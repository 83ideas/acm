/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { NbMenuItem } from '@nebular/theme';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {

  getMenu(): Observable<NbMenuItem[]> {
    const dashboardMenu: NbMenuItem[] = [
      {
        title: 'My Profile',
        icon: 'home-outline',
        link: '/pages/users/current/',
        home: true,
        children: undefined,
      },
      {
        title: 'Organizations',
        icon: 'home-outline',
        link: '/pages/organizations',
        home: true,
        children: undefined,
      },
      {
        title: 'Event Details',
        icon: 'home-outline',
        link: '/pages/events/',
        home: true,
        children: undefined,
      },
      {
        title: 'Event Website',
        icon: 'keypad-outline',
        link: '/pages/ui-features',
      
       children: [
          {
            title: 'Tickets',
            link: '/pages/tickets',
          },
          {
            title: 'Landing Page',
            link: '/pages/landingpage',
          },
          {
            title: 'Registration',
            link: '/pages/modal-overlays/window',
          },
        ],
      },
	  {
        title: 'Event Areas',
        icon: 'keypad-outline',
        link: '/pages/ui-features',
       children: [
          {
            title: 'Reception',
            link: '/pages/reception',
          },
          {
            title: 'Stage',
            link: '/pages/stage',
          },
		  {
            title: 'Networking',
            link: '/pages/networking',
          },
		  {
            title: 'Sessions',
            link: '/pages/sessions',
          },
		  {
            title: 'Expo',
            link: '/pages/exhibitors',
          },
        ],
      },
    ];

    const menu: NbMenuItem[] = [
      
      {
        title: 'Billing',
        icon: 'layout-outline',
        
      },
      {
        title: 'Team',
        icon: 'edit-2-outline',
        
      },
	   {
        title: 'Other',
        icon: 'keypad-outline',
        link: '/pages/ui-features',
       children: [
          {
            title: 'Speakers',
            link: '/pages/speakers',
          },
          {
            title: 'Sessions',
            link: '/pages/modal-overlays/window',
          },
          {
            title: 'Sponsors',
            link: '/pages/sponsors',
          },
        ],
      },
      {
        title: 'Attendees',
        icon: 'keypad-outline',
        link: '/pages/attendees',
       children: [
          {
            title: 'View Attendees',
            link: '/pages/attendees',
          },
          {
            title: 'Add Attendees',
            link: '/pages/addattendees',
          },
          {
            title: 'E-mail Attendees',
            link: '/pages/modal-overlays/popover',
          },
        ],
      },
       
      {
        title: 'Settings',
        icon: 'message-circle-outline',
        children: [
          {
            title: 'General',
            link: '/pages/extra-components/calendar',
          },
          {
            title: 'Integrations',
            link: '/pages/extra-components/progress-bar',
          },
         
        ],
      },
      
    ];

    return of([...dashboardMenu, ...menu]);
  }
}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { DataService } from '../../services/data.service'; 

@Component({
  selector: 'ngx-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {
  arrstages: any=[];
  userDisplayID = 1;
  id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      stream_provider: {
        title: 'Stream Provider',
        type: 'string',
      },
      stream_key: {
        title: 'Stream Key',
        type: 'varchar',
      },
      RTMP_url: {
        title: 'RTMP url',
        type: 'varchar',
      },
      segment_backstage_link: {
        title: 'Segment Backstage Link',
        type: 'varchar',
      },
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },
    },
  };
  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router) {
    const data = this.service.getData();
    this.source.load(data);
   }

  ngOnInit(): void {
    this.showstages(this.userDisplayID);
  }
  showstages(userDisplayID){
   
    this.dataService.showstages( this.userDisplayID).subscribe(res=>{
      this.arrstages = res;
	  console.log(this.arrstages);
  })
  }
  updatestage(event) {

    var data = {"stream_provider": event.newData.stream_provider,
                "stream_key" : event.newData.stream_key,
                "RTMP_url" : event.newData.RTMP_url,
                "segment_backstage_link" : event.newData.segment_backstage_link,
                "org_id":"1",
				"event_id":"1",
				"managerid":"1"
                };
                console.log(data);
  return this.http.post('https://server.kodeshode.com/api/updatestage/'+event.newData.id, data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.showstages(this.userDisplayID);
      });
  }
  deletestage(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('https://server.kodeshode.com/api/deletestage/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.showstages(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
  
 }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addspeakers',
  templateUrl: './addspeakers.component.html',
  styleUrls: ['./addspeakers.component.scss']
})
export class AddspeakersComponent implements OnInit {

  public speaker = {
	
    first_name: null,
    last_name: null,
    email: null,
    title: null,
    profile_image: null,
    company: null,
    biography: null,
    linkdin: null,
    twitter: null,
    facebook: null,
      session_id:1,
      event_id:1,
      managerid:1
    }
    
    userDisplayID='';
    speakersForm: FormGroup;
    files: any;
    constructor(private dataService:DataService,private router: Router,) { }
  
    ngOnInit(): void {
      this.speakersForm = new FormGroup({
        first_name : new FormControl(),
        last_name : new FormControl(),
        email : new FormControl(),
        title : new FormControl(),
        profile_image : new FormControl(),
        company : new FormControl(),
        biography : new FormControl(),
        linkdin : new FormControl(),
        twitter : new FormControl(),
        facebook : new FormControl(),
        session_id: new FormControl(6),
        event_id: new FormControl(13),
        managerid: new FormControl(5),
     });
    }
    addspeakers()
    {
  
      this.speaker = this.speakersForm.value;
      let formdata = new FormData;
      formdata.append("first_name",this.speakersForm.get('first_name').value);
      formdata.append("last_name",this.speakersForm.get('last_name').value);
      formdata.append("email",this.speakersForm.get('email').value);
      formdata.append("title",this.speakersForm.get('title').value);
      formdata.append("profile_image",this.speakersForm.get('profile_image').value);
      formdata.append("company",this.speakersForm.get('company').value);
      formdata.append("biography",this.speakersForm.get('biography').value);
      formdata.append("linkdin",this.speakersForm.get('linkdin').value);
      formdata.append("twitter",this.speakersForm.get('twitter').value);
      formdata.append("facebook",this.speakersForm.get('facebook').value);
      formdata.append("session_id",this.speakersForm.get('session_id').value);
      formdata.append("event_id",this.speakersForm.get('event_id').value);
      formdata.append("managerid",this.speakersForm.get('managerid').value);
   
      this.dataService.addspeaker(formdata).subscribe(res=>{
          console.log(res);
          return this.router.navigateByUrl('/pages/speakers');
      })
       
    }
    imageupload(event){
     if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.speakersForm.get('profile_image').setValue(file);
      console.log(this.speakersForm.get('profile_image').value);
    }
  }

}

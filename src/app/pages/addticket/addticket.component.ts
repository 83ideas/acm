import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-addticket',
  templateUrl: './addticket.component.html',
  styleUrls: ['./addticket.component.scss']
})
export class AddticketComponent implements OnInit {
  public ticket = {
    ticket_name : null,
    ticket_type : null,
    ticket_price : null,
    description:null,
    ticket_count:null,
    hide_ticket:null,
    org_id:1,
    event_id:1,
    managerid:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  ticketForm: FormGroup;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.ticketForm = new FormGroup({
      ticket_name: new FormControl(),
      ticket_type: new FormControl(),
      ticket_price: new FormControl(),
      description: new FormControl(),
      ticket_count: new FormControl(),
      hide_ticket: new FormControl(),
      org_id: new FormControl(1),
      event_id: new FormControl(1),
      managerid: new FormControl(1),
   });
  }
  addticket()
  {
    this.ticket = this.ticketForm.value;
    this.dataService.addticket(this.ticket).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/tickets');
    })
    
  }
  

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./addevent.component.scss']
})
export class AddeventComponent implements OnInit {
 
  public event = {
    title : null,
    start_date : null,
    end_date : null,
    email_id:null,
    photo:null,
    client_id:null,
    organization_id:null,
    stage:0,
    sessions:0,
    networking:0,
    expo:0,
    
  }
  
    userDisplayID='';
   eventForm: FormGroup;
    files: any;
    constructor(private dataService:DataService,private router: Router,) { }
  
    ngOnInit(): void {
      this.eventForm = new FormGroup({
        title : new FormControl(),
        start_date : new FormControl(),
        end_date : new FormControl(),
        email_id : new FormControl(),
        photo : new FormControl(),
        client_id : new FormControl(sessionStorage.getItem('loggedUserid')),
        organization_id : new FormControl(4),
        stage : new FormControl(0),
        sessions : new FormControl(0),
        networking : new FormControl(0),
        expo : new FormControl(0),
        
     });
    }
    addevent()
    {
  
      this.event = this.eventForm.value;
      let formdata = new FormData;
      formdata.append("title",this.eventForm.get('title').value);
      formdata.append("start_date",this.eventForm.get('start_date').value);
      formdata.append("end_date",this.eventForm.get('end_date').value);
      formdata.append("email_id",this.eventForm.get('email_id').value);
      formdata.append("photo",this.eventForm.get('photo').value);
      formdata.append("organization_id",this.eventForm.get('organization_id').value);
      formdata.append("client_id",this.eventForm.get('client_id').value);
      formdata.append("stage",this.eventForm.get('stage').value);
      formdata.append("sessions",this.eventForm.get('sessions').value);
      formdata.append("networking",this.eventForm.get('networking').value);
      formdata.append("expo",this.eventForm.get('expo').value);
      
      console.log(this.eventForm.get('stage').value);
      this.dataService.addevent(formdata).subscribe(res=>{
          console.log(res);
          return this.router.navigateByUrl('/pages/events');
      })
       
    }
    stageStatus(event){
      if(event.target.checked == true){
          this.event.stage = 1;
          this.eventForm.get('stage').setValue(1);
        }
        else{
          this.event.stage = 0;
          this.eventForm.get('stage').setValue(0);
        }
       console.log(this.event);
    }
    networkingStatus(event){
      if(event.target.checked == true){
        this.event.networking = 1;
        this.eventForm.get('networking').setValue(1);
      }
      else{
        this.event.networking = 0;
        this.eventForm.get('networking').setValue(0);
      }
     console.log(this.event);
    }
    sessionStatus(event){
      if(event.target.checked == true){
        this.event.sessions = 1;
        this.eventForm.get('sessions').setValue(1);
      }
      else{
        this.event.sessions = 0;
        this.eventForm.get('sessions').setValue(0);
      }
     console.log(this.event);
    }
    expoStatus(event){
      if(event.target.checked == true){
        this.event.expo = 1;
        this.eventForm.get('expo').setValue(1);
      }
      else{
        this.event.expo = 0;
        this.eventForm.get('expo').setValue(0);
      }
     console.log(this.event);
    }

    imageupload(event){
     if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.eventForm.get('photo').setValue(file);
      console.log(this.eventForm.get('photo').value);
      
    }
  }

}

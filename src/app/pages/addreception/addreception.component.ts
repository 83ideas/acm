import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addreception',
  templateUrl: './addreception.component.html',
  styleUrls: ['./addreception.component.scss']
})
export class AddreceptionComponent implements OnInit {

  public recep = {
    reception_data : null,
    event_banner : null,
    announcement_feed : null,
    show_sponsors:null,
    welcome_video_link:null,
    org_id:1,
    event_id:1,
    managerid:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  receptionForm: FormGroup;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.receptionForm = new FormGroup({
      reception_data: new FormControl(),
      event_banner: new FormControl(),
      announcement_feed: new FormControl(),
      show_sponsors: new FormControl(),
      welcome_video_link: new FormControl(),
      org_id: new FormControl(1),
      event_id: new FormControl(1),
      managerid: new FormControl(1),
   });
  }
  addreception()
  {
    this.recep = this.receptionForm.value;
    this.dataService.addreception(this.recep).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/reception');
    })
    
}
}

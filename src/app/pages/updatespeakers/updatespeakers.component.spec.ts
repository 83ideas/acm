import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatespeakersComponent } from './updatespeakers.component';

describe('UpdatespeakersComponent', () => {
  let component: UpdatespeakersComponent;
  let fixture: ComponentFixture<UpdatespeakersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatespeakersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatespeakersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

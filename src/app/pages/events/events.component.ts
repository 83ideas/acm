import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { LocalDataSource } from 'ng2-smart-table';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { ActivatedRoute, Router } from '@angular/router';
import { PagesMenu } from '../pages-menu';
import { PagesComponent } from '../pages.component';

@Component({
  selector: 'ngx-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  arrevents: any=[];
  arrorg: any=[];
  userDisplayID = '';
  id:any;
  org_id:any;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
        addable: false,
      },
      title: {
        title: 'Title',
        type: 'string',
      },
      start_date: {
        title: 'Start Date',
        type: 'date',
      },
      end_date: {
        title: 'End Date',
        type: 'date',
      },
      email_id: {
        title: 'E-mail',
        type: 'string',
      },
      photo: {
        title: 'Image',
        type: 'file',
       
      },
    },
  }; 

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private route:ActivatedRoute,private router: Router,private hide_menu: PagesMenu,
    private load_again: PagesComponent) {
    const data = this.service.getData();
    this.source.load(data);
  }
  ngOnInit(): void {
    this.userDisplayID = sessionStorage.getItem('loggedUserid');
    this.id= this.route.snapshot.queryParams["org"];
    console.log(this.id);
    if(this.id){
      this.getallevents(this.id);
     
    }else{
    this.getevents(this.userDisplayID);
    }
    this.getorg(this.userDisplayID);
  }
  getallevents(userDisplayID){
   
    this.dataService.getallevents( this.id).subscribe(res=>{
      this.arrevents = res;
	  console.log(this.arrevents);
  })
  }
  getevents(userDisplayID){
   
    this.dataService.getevents( this.userDisplayID).subscribe(res=>{
      this.arrevents = res;
	  console.log(this.arrevents);
  })
  }
   
  getorg(userDisplayID){
    this.dataService.getorg(this.userDisplayID).subscribe(res=>{
      //this.source.load(res);  
     // this.arrorg = res as arrorg[];
      this.arrorg = res;
      console.log(this.arrorg);
  })
  }
  navigateTo(value) {
   
    console.log(value);
    this.org_id = value;
    if (value) {
       this.router.navigateByUrl('/pages/events?org='+value);
    }
    
    return false;
}

  addEvent(event) {
    
    var data = {"title" : event.newData.title,
                "organization_id" : this.org_id,
                "start_date" : event.newData.start_date,
                "end_date" : event.newData.end_date,
                "email_id" : event.newData.email_id,
                "client_id" : sessionStorage.getItem('loggedUserid'),
                "photo":  "events_bricks.jpg"

                };
                return this.http.post('http://13.244.115.67/server/api/createnewevent',data).subscribe(
        res => {
          console.log(res);
          console.log('hello');
          event.confirm.resolve(event.newData);
          this.getallevents(this.userDisplayID);
      });
  }
  
  updateEvent(event) {
    console.log('event');
    //var data = return this.http.get('https://server.kodeshode.com/api/getorganization/'+event.data.id);
    var data = {"eventid": event.newData.id,
                "title" : event.newData.title,
                "organization_id" : 13,
                "start_date" : event.newData.start_date,
                "end_date" : event.newData.end_date,
                "email_id" : event.newData.email_id,
                "client_id" : sessionStorage.getItem('loggedUserid'),
                "photo": "events_bricks.jpg",
                "notes": "testing"
                };
                console.log(data);
  return this.http.post('http://13.244.115.67/server/api/updateevent', data).subscribe(
        res => {
          console.log(res);
          event.confirm.resolve(event.newData);
          this.getallevents(this.userDisplayID);
      });
  }

  deleteEvent(event){
    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event.data);
      return this.http.get('http://13.244.115.67/server/api/deleteevent/'+event.data.id).subscribe(res=>{ 
           console.log(res);
           event.confirm.resolve(event.source.data);
           this.getallevents(this.userDisplayID);
       });
    } else {
      event.confirm.reject();
    }
    
   //event.confirm.resolve(event.source.data);

 }
 
}

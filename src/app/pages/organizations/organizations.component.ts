import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { LocalDataSource } from 'ng2-smart-table';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { SmartTableData } from '../../@core/interfaces/common/smart-table';
import { Router } from '@angular/router';




@Component({

  selector: 'ngx-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit {
  arrorg: any=[];
  userDisplayID = '' ;
  
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      
      title: {
        title: 'Title',
        type: 'string',
      },
      contact_person: {
        title: 'Contact Person',
        type: 'string',
      },
     
      email_id: {
        title: 'Email',
        type: 'string',
      },
      contact_number: {
        title: 'Contact Number',
        type: 'number',
      },
      
      deleted_at: {
        title: 'Delete Status',
        type: 'date',
        editable:false,
        addable: false,
      },

      id: {
        title: 'Events',
        type: 'html',
        filter: false,
        valuePrepareFunction: (id: string) => {
        return '<a onclick="onCustom($event)" href="/pages/events?org=' + id + '">List</a>'; // Complete';
}
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();
  public CreatedOrg: boolean;
  constructor(private service: SmartTableData,private dataService:DataService, private http:HttpClient,private router:Router) {
    //const data = this.dataService.getorg(this.userDisplayID);
    //this.source.load(data);
  }
  ngOnInit(): void {
    this.userDisplayID = sessionStorage.getItem('loggedUserid');
    this.getorg(this.userDisplayID);
  }

  onCustom(event) { this.router.navigateByUrl('/pages/events?org=' + event.data.id); }

  getorg(userDisplayID){
    this.dataService.getorg(this.userDisplayID).subscribe(res=>{
      //this.source.load(res);  
     // this.arrorg = res as arrorg[];
      this.arrorg = res;
      console.log(this.arrorg);
  })
  }

  addRecord(event) {
    var data = {"title" : event.newData.title,
                "contact_person" : event.newData.contact_person,
                "contact_number" : event.newData.contact_number,
                "email_id" : event.newData.email_id,
                "clientid" : sessionStorage.getItem('loggedUserid')
                };
                return this.http.post('http://13.244.115.67/server/api/createneworg',data).subscribe(
        res => {
          this.CreatedOrg = true;
          console.log(res);
          
          event.confirm.resolve(event.newData);
          this.getorg(this.userDisplayID);
      });
  }

  deleteRecord(event){
    console.log(event.data);
    return this.http.get('http://13.244.115.67/server/api/deleteorganization/'+event.data.id).subscribe(res=>{ 
         console.log(res);
         event.confirm.resolve(event.source.data);
         this.getorg(this.userDisplayID);
     });
   //event.confirm.resolve(event.source.data);

 }
 updateRecord(event) {
  console.log('ddddd');
  //var data = return this.http.get('https://server.kodeshode.com/api/getorganization/'+event.data.id);
  var data = {"orgid" : event.newData.id,
    "title" : event.newData.title,
              "contact_person" : event.newData.contact_person,
             "email_id" : event.newData.email_id,
              "contact_number": event.newData.contact_number
              };
              console.log(data);
return this.http.post('http://13.244.115.67/server/api/updateorg', data).subscribe(
      res => {
        console.log(res);
        event.confirm.resolve(event.newData);
        this.getorg(this.userDisplayID);
    });
}
}
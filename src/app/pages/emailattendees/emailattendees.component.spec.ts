import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailattendeesComponent } from './emailattendees.component';

describe('EmailattendeesComponent', () => {
  let component: EmailattendeesComponent;
  let fixture: ComponentFixture<EmailattendeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailattendeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailattendeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

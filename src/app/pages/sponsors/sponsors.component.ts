import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent implements OnInit {

  [x: string]: any;
 
  userDisplayID = '';
  public loggedIn: boolean;
  constructor(private dataService:DataService) { }
  arrsponsors: any=[];
 
  ngOnInit(): void {
    this.showsponsor(this.userDisplayID);
  }
  showsponsor(userDisplayID){
    this.dataService.showsponsor(this.userDisplayID).subscribe(res=>{
     var response2 = res;
  
      console.log( response2 );
      this.arrsponsors = response2;
  })
  }
  deletesponsor(id){
    this.dataService.deletesponsor(id).subscribe(res=>{
      this.showsponsor(this.userDisplayID);
      console.log(id);
    })
    
  }
 

}

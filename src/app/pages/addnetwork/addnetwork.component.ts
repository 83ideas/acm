import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addnetwork',
  templateUrl: './addnetwork.component.html',
  styleUrls: ['./addnetwork.component.scss']
})
export class AddnetworkComponent implements OnInit {

  public network = {
	
    max_meeting_duration: null,
    min_meeting_duration: null,
      event_id:null,
      managerid:null
    }
    
    userDisplayID='';
    networkForm: FormGroup;
    files: any;
    constructor(private dataService:DataService,private router: Router,) { }
  
    ngOnInit(): void {
      this.networkForm = new FormGroup({
        max_meeting_duration : new FormControl(),
        min_meeting_duration : new FormControl(),
        event_id: new FormControl(13),
        managerid: new FormControl(sessionStorage.getItem('loggedUserid')),
     });
    }
    addnetwork()
    {
  
      this.network = this.networkForm.value;
      
      this.dataService.addnetwork(this.network).subscribe(res=>{
          console.log(res);
          return this.router.navigateByUrl('/pages/networking');
      })
       
    }
}

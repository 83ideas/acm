import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnetworkComponent } from './addnetwork.component';

describe('AddnetworkComponent', () => {
  let component: AddnetworkComponent;
  let fixture: ComponentFixture<AddnetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

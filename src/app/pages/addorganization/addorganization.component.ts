import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addorganization',
  templateUrl: './addorganization.component.html',
  styleUrls: ['./addorganization.component.css']
})
export class AddorganizationComponent implements OnInit {
  public org = {
    title : null,
    contact_person : null,
    contact_number : null,
    email_id:null,
    client_id:null,
    notes:null,
    photo:null,
	
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  orgForm: FormGroup;
   files: any;
  constructor(private dataService:DataService,private Auth: AuthService,private router: Router,){}
  

  ngOnInit(): void {
    
    this.orgForm = new FormGroup({
      title: new FormControl(),
      contact_person: new FormControl(),
      contact_number: new FormControl(),
      email_id: new FormControl(),
      client_id: new FormControl(sessionStorage.getItem('loggedUserid')),
      notes: new FormControl(),
      photo: new FormControl(),
   });
  }
  addorg()
  {
    this.org = this.orgForm.value;
    let formdata = new FormData;
	formdata.append("title",this.orgForm.get('title').value);
	formdata.append("contact_person",this.orgForm.get('contact_person').value);
	formdata.append("contact_number",this.orgForm.get('contact_number').value);
	formdata.append("email_id",this.orgForm.get('email_id').value);
	formdata.append("client_id",this.orgForm.get('client_id').value);
	formdata.append("notes",this.orgForm.get('notes').value);
	formdata.append("photo",this.orgForm.get('photo').value);
	
    this.dataService.addorg(formdata).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/organizations');
    })
    
  }
   imageupload(event){
     if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.orgForm.get('photo').setValue(file);
      console.log(this.orgForm.get('photo').value);
    }
  }
  
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-updatenetwork',
  templateUrl: './updatenetwork.component.html',
  styleUrls: ['./updatenetwork.component.scss']
})
export class UpdatenetworkComponent implements OnInit {

  public network = {
    max_meeting_duration : null,
    min_meeting_duration : null,
    event_id:13,
	managerid:5,
  }
  id:any;
  data: any;
  networkForm: FormGroup;
  files: any;

  constructor(private route:ActivatedRoute,
    private dataservice:DataService,
    private router: Router) { }

  ngOnInit(): void {
    this.id= this.route.snapshot.params.id;
    this.getnetwork();
  }
  getnetwork(){
    this.dataservice.singlenetwork(this.id).subscribe(res=>{
      this.data = res;
      console.log(this.data);
      this.network.max_meeting_duration = this.data.max_meeting_duration;
      this.network=this.data;
      console.log( this.id);
      console.log(this.network.max_meeting_duration);
    })
  }
  updatenetwork(){
    this.network = this.networkForm.value;
    this.dataservice.updatenetwork(this.id,this.network).subscribe(res=>{
      this.router.navigateByUrl('/pages/networking');
    })
  }

}

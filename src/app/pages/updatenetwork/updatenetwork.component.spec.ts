import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatenetworkComponent } from './updatenetwork.component';

describe('UpdatenetworkComponent', () => {
  let component: UpdatenetworkComponent;
  let fixture: ComponentFixture<UpdatenetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatenetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatenetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

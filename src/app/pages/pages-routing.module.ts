/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { OrganizationsComponent } from './organizations/organizations.component';
import { AddorganizationComponent } from './addorganization/addorganization.component';
import { EventsComponent } from './events/events.component';
import { TicketsComponent } from './tickets/tickets.component';
import { AddticketComponent } from './addticket/addticket.component';
import { ReceptionComponent } from './reception/reception.component';
import { StageComponent } from './stage/stage.component';
import { SessionsComponent } from './sessions/sessions.component';
import { AddreceptionComponent } from './addreception/addreception.component';
import { AddstageComponent } from './addstage/addstage.component';
import { AddsessionComponent } from './addsession/addsession.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { AddlandingpageComponent } from './addlandingpage/addlandingpage.component';
import { UpdatelandingComponent } from './updatelanding/updatelanding.component';
import { ExhibitorsComponent } from './exhibitors/exhibitors.component';
import { AddexhibitorsComponent } from './addexhibitors/addexhibitors.component';
import { AddrepresentativesComponent } from './addrepresentatives/addrepresentatives.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { AttendeesComponent } from './attendees/attendees.component';
import { AddspeakersComponent } from './addspeakers/addspeakers.component';
import { AddsponsorsComponent } from './addsponsors/addsponsors.component';
import { AddattendeesComponent } from './addattendees/addattendees.component';
import { UpdatespeakersComponent } from './updatespeakers/updatespeakers.component';
import { UpdateattendeesComponent } from './updateattendees/updateattendees.component';
import { UpdatesponsorComponent } from './updatesponsor/updatesponsor.component';
import { AddeventComponent } from './addevent/addevent.component';
import { NetworkingComponent } from './networking/networking.component';
import { AddnetworkComponent } from './addnetwork/addnetwork.component';
import { UpdatenetworkComponent } from './updatenetwork/updatenetwork.component';
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'organizations',
      component: OrganizationsComponent,
    },
    {
      path: 'events',
      component: EventsComponent,
    },
    {
      path: 'tickets',
      component: TicketsComponent,
    },
    {
      path: 'reception',
      component: ReceptionComponent,
    },
    {
      path: 'stage',
      component: StageComponent,
    },
    {
      path: 'sessions',
      component: SessionsComponent,
    },
    {
      path: 'landingpage',
      component: LandingpageComponent,
    },
    {
      path: 'exhibitors',
      component: ExhibitorsComponent,
    },
    {
      path: 'speakers',
      component: SpeakersComponent,
    },
	{
      path: 'sponsors',
      component: SponsorsComponent,
    },
	{
      path: 'attendees',
      component: AttendeesComponent,
    },
    {
      path: 'networking',
      component: NetworkingComponent,
    },
    {
      path: 'addnetwork',
      component: AddnetworkComponent,
    },
	{
      path: 'addspeakers',
      component: AddspeakersComponent,
    },
    {
      path: 'addevent',
      component: AddeventComponent,
    },
	{
      path: 'addsponsors',
      component: AddsponsorsComponent,
    },{
      path: 'addattendees',
      component: AddattendeesComponent,
    },
	{
      path: 'updatespeakers/:id',
      component: UpdatespeakersComponent,
    },
    {
      path: 'updatenetwork/:id',
      component: UpdatenetworkComponent,
    },
	{
      path: 'updatesponsors/:id',
      component: UpdatesponsorComponent,
    },
	{
      path: 'updateattendees/:id',
      component: UpdateattendeesComponent,
    },
    {
      path: 'addexhibitors',
      component: AddexhibitorsComponent,
    },
    {
      path: 'addrepresentatives',
      component: AddrepresentativesComponent,
    },
    {
      path: 'addorganization',
      component: AddorganizationComponent,
    },
    {
      path: 'addticket',
      component: AddticketComponent,
    },
    {
      path: 'addlandingpage',
      component: AddlandingpageComponent,
    },
    {
      path: 'updatelanding/:id',
      component: UpdatelandingComponent,
    },
    {
      path: 'addreception',
      component: AddreceptionComponent,
    },
    {
      path: 'addstage',
      component: AddstageComponent,
    },
    {
      path: 'addsession',
      component: AddsessionComponent,
    },
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users.module')
        .then(m => m.UsersModule),
    },
    {
      path: 'addticket',
      loadChildren: () => import('./addticket/addticket.module')
        .then(m => m.AddticketModule),
    },
    {
      path: 'addevent', 
      loadChildren: () => import('./addevent/addevent.module')
        .then(m => m.AddeventModule),
    },
    {
      path: 'addlandingpage',
      loadChildren: () => import('./addlandingpage/addlandingpage.module')
        .then(m => m.AddlandingpageModule),
    }, 
    {
      path: 'addexhibitors',
      loadChildren: () => import('./addexhibitors/addexhibitors.module')
        .then(m => m.AddexhibitorsModule), 
    },
    {
      path: 'addrepresentatives',
      loadChildren: () => import('./addrepresentatives/addrepresentatives.module')
        .then(m => m.AddrepresentativesModule),
    }, 
    {
      path: 'updatelanding/:id',
      loadChildren: () => import('./updatelanding/updatelanding.module')
        .then(m => m.UpdatelandingModule),
    }, 
    {
      path: 'addreception',
      loadChildren: () => import('./addreception/addreception.module')
        .then(m => m.AddreceptionModule),
    },
    {
      path: 'addstage',
      loadChildren: () => import('./addstage/addstage.module')
        .then(m => m.AddstageModule),
    },
    {
      path: 'addsession',
      loadChildren: () => import('./addsession/addsession.module')
        .then(m => m.AddsessionModule),
    },
    {
      path: 'addorganization',
      loadChildren: () => import('./addorganization/addorganization.module')
        .then(m => m.AddorganizationModule),
    },
    {
      path: 'addnetwork',
      loadChildren: () => import('./addnetwork/addnetwork.module')
        .then(m => m.AddnetworkModule),
    },
    {
      path: 'organizations',
      loadChildren: () => import('./organizations/organizations.module')
        .then(m => m.OrganizationsModule),
    },
    {
      path: 'networking',
      loadChildren: () => import('./networking/networking.module')
        .then(m => m.NetworkingModule),
    },
    {
      path: 'events',
      loadChildren: () => import('./events/events.module')
        .then(m => m.EventsModule),
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
    },
    {
      path: 'tickets',
      loadChildren: () => import('./tickets/tickets.module')
        .then(m => m.TicketsModule),
    },
    {
      path: 'speakers',
      loadChildren: () => import('./speakers/speakers.module')
        .then(m => m.SpeakersModule),
    },
	{
      path: 'sponsors',
      loadChildren: () => import('./sponsors/sponsors.module')
        .then(m => m.SponsorsModule),
    },
	{
      path: 'attendees',
      loadChildren: () => import('./attendees/attendees.module')
        .then(m => m.AttendeesModule),
    },
	{
      path: 'addspeakers',
      loadChildren: () => import('./addspeakers/addspeakers.module')
        .then(m => m.AddspeakersModule),
    },
	{
      path: 'addsponsors',
      loadChildren: () => import('./addsponsors/addsponsors.module')
        .then(m => m.AddsponsorsModule),
    },
	{
      path: 'addattendees',
      loadChildren: () => import('./addattendees/addattendees.module')
        .then(m => m.AddattendeesModule),
    },
	{
      path: 'updatespeakers/:id',
      loadChildren: () => import('./updatespeakers/updatespeakers.module')
        .then(m => m.UpdatespeakersModule),
    },
    {
      path: 'updatenetwork/:id',
      loadChildren: () => import('./updatenetwork/updatenetwork.module')
        .then(m => m.UpdatenetworkModule),
    },
	{
      path: 'updatesponsors/:id',
      loadChildren: () => import('./updatesponsor/updatesponsor.module')
        .then(m => m.UpdatesponsorModule),
    },
	{
      path: 'updateattendees/:id',
      loadChildren: () => import('./updateattendees/updateattendees.module')
        .then(m => m.UpdateattendeesModule),
    },
    {
      path: 'reception',
      loadChildren: () => import('./reception/reception.module')
        .then(m => m.ReceptionModule),
    },
    {
      path: 'stage',
      loadChildren: () => import('./stage/stage.module')
        .then(m => m.StageModule),
    },
    {
      path: 'session',
      loadChildren: () => import('./sessions/sessions.module')
        .then(m => m.SessionsModule),
    },
    {
      path: 'landingpage',
      loadChildren: () => import('./landingpage/landingpage.module')
        .then(m => m.LandingpageModule),
    },
    {
      path: 'exhibitors',
      loadChildren: () => import('./exhibitors/exhibitors.module')
        .then(m => m.ExhibitorsModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },
    {
      path: 'editors',
      loadChildren: () => import('./editors/editors.module')
        .then(m => m.EditorsModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

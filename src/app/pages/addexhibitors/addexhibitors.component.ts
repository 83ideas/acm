import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'ngx-addexhibitors',
  templateUrl: './addexhibitors.component.html',
  styleUrls: ['./addexhibitors.component.scss']
})
export class AddexhibitorsComponent implements OnInit {

  public expo = {
    exibit_name : null,
    email : null,
    title:null,
    description:null,
    stream_provider:null,
    booth_size:null,
    expo_banner:null,
    expo_logo:null,
    promo_video_url:null,
    linkdin:null,
    twitter:null,
    facebook:null,
    event_id:1,
    managerid:1
  }
  
  public loggedIn: boolean;
  userDisplayID='';
  expoForm: FormGroup;
  files: any;
  constructor(private dataService:DataService,private router: Router,) { }

  ngOnInit(): void {
    this.expoForm = new FormGroup({
     
      exibit_name : new FormControl(),
      email : new FormControl(),
      title:new FormControl(),
      description:new FormControl(),
      stream_provider:new FormControl(),
      booth_size:new FormControl(),
      expo_banner:new FormControl(),
      expo_logo:new FormControl(),
      promo_video_url:new FormControl(),
      linkdin:new FormControl(),
      twitter:new FormControl(),
      facebook:new FormControl(),
      event_id:new FormControl(1),
      managerid:new FormControl(1),
   });
  }
  addexpo()
  {

    this.expo = this.expoForm.value;
    this.dataService.addexpo(this.expo).subscribe(res=>{
        console.log(res);
        return this.router.navigateByUrl('/pages/exhibitors');
    })
     
  }
  imageupload(event){
    this.files=event.target.files[0];
    this.expoForm.get('expo_banner').setValue(this.files);
}

}

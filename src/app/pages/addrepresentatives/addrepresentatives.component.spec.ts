import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrepresentativesComponent } from './addrepresentatives.component';

describe('AddrepresentativesComponent', () => {
  let component: AddrepresentativesComponent;
  let fixture: ComponentFixture<AddrepresentativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrepresentativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrepresentativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
